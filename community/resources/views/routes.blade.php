<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <title>ROUTES</title>
    </head>
    <body>
        <h1>Usando GET</h1>
        <form action="routes " method="GET">
            <input type="text" name="search">
            <input type="submit" value="BUSCAR">
        </form>
        <h1>Usando POST</h1>
        <form action="routes " method="POST">
            <input type="text" name="search">
            <input type="submit" value="BUSCAR">
        </form>
    </body>
</html>        