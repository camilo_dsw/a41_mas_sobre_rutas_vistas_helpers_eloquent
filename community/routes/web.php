<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\UserController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

//Auth::routes();
Auth::routes(['verify' => 'true']);
Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

Route::group(['middleware' => 'verified'], function () {

// Rutas a verificar


});

//para mostrar todos los links que llamará al método index mediante GET
Route::get('community', [App\Http\Controllers\CommunityLinkController::class, 'index']);
//para crear un link que llamará al método store del controlador mediante POST
Route::post('community', [App\Http\Controllers\CommunityLinkController::class, 'store']);


// 1. Crea una ruta que tenga un parámetro que sea opcional 
// 2. Crea una ruta que tenga un parámetro que sea opcional 
// y tenga un valor por defecto en caso de que no se especifique.


//4. Crea una ruta que atienda por GET y por POST (en un único método)


// creo una ruta que me lleve a una vista para hacer el ejemplo 4
Route::get('/', function () {
    return view('routes');
});

//creamos una ruta de tipo MATCH que me va a retornar el dato que tenga en el campo buscar
Route::match(['get', 'post'], '/routes', function () {
    return request('search');
});




//5. Crea una ruta que compruebe que un parámetro está formado sólo por números.
/*
Route::get('informatica/{entrada}', function($entrada){
    if($entrada){
        $numero=0;
        $numero=is_numeric($entrada);
        if($numero==1){

            return "La entrada $entrada, es un número" ;
        }else{
            return "La entrada $entrada, no es un número";
            }

  
    }
});
*/




// 6. Crea una ruta con dos parámetros que compruebe que el primero está formado 
// sólo por letras y el segundo sólo por números.

Route::get('informatica/{numero}/{texto}', function($numero, $texto){
    $resultado="";
    if($numero){
        $aux=0;
        $aux=is_numeric($numero);
        if($aux==1){

            $resultado= "La entrada $numero, es un número y " ;
        }else{
            $resultado= "La entrada $numero, no es un número y ";
            }

  
    }

    if($texto){

        $longitud=strlen($texto);
        $indice=0;
        $flag=1;
        while($longitud > $indice+1)
        {
            if($texto[$indice]<97) $flag=0;
            $indice++;
        }

        if($flag){
            $resultado_texto= " El texto $texto solo tiene letras.";
        }else{

            $resultado_texto= " El texto $texto contiene caracteres que no son letras";
        }

      
    }

    

    return $resultado.$resultado_texto;

    
});

// Utiliza el helper env para que cuando se acceda a la
// ruta /host nos devuleva la dirección IP donde se encuentra 
//la base de datos de nuestro proyecto.

Route::get('host', function(){
    return env('DB_CONNECTION');
});



//Utiliza el helper config para que cuando se acceda a la ruta /timezone 
//se muestre la zona horaria.


Route::get('timezone', function(){
    return config('app.timezone');
});


// Define una vista llamada home.blade.php que muestre "Esta es mi primera vista en Laravel" 
// al acceder a la ruta /inicio de tu proyecto. Utiliza Route::view.

Route::view('/inicio','home2');

// Crea otra vista que se llame fecha.blade.php y crea una ruta en /fecha. 
// La ruta le pasará a la vista un array asociativo para que se muestre 
// la fecha sacando por pantalla las variables de dicho array. 
//El día estará en una variable, el mes en otra y el año en otra 


Route::get('fecha', function(){
    
    return view('fecha',['dia'=>'12','mes'=>'enero','ano'=>'2022']);

});

//Haz lo mismo pero con la función PHP compact.

Route::get('fecha2', function(){

    $dia = '27';
    $mes = 'enero';
    $ano = '2022';

    $fecha = compact('dia','mes','ano');

    return view('fecha', $fecha);
    
    

});



// Haz lo mismo pero con el helper with.

Route::get('fecha3', function(){
    
    return view('fecha')->with('dia', '21')
                        ->with('mes', 'enero')
                        ->with('ano', '2022');


});


// Cargar imágenes desde blade. Crea una carpeta images en el 
//directorio public y dentro sube una imagen 404.jpg personalizada.


Route::view('/imagen','imagen404');


//Crea otro usuario pero con el método create del modelo 
//(User::create(array asociativo con los valores)).


Route::get('crear', [UserController::class, 'create']);




//3. Crea una ruta que atienda por POST y compruébala con Postman
//Route::get('informatica/crear',[App\Http\Controllers\HomeController::class, 'store']);


//5. Crea una ruta que compruebe que un parámetro está formado sólo por números.

//Route::get('informatica/store2',[App\Http\Controllers\HomeController::class, 'store2']);